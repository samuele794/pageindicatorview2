package it.opensource.samuele794.pageindicatorview2.animation.data.type

import it.opensource.samuele794.pageindicatorview2.animation.data.Value

class ThinWormAnimationValue : WormAnimationValue(), Value {

    var height: Int = 0
}
