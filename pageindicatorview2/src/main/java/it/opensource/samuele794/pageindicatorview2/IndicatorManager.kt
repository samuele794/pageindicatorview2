package it.opensource.samuele794.pageindicatorview2

import it.opensource.samuele794.pageindicatorview2.animation.AnimationManager
import it.opensource.samuele794.pageindicatorview2.animation.controller.ValueController
import it.opensource.samuele794.pageindicatorview2.animation.data.Value
import it.opensource.samuele794.pageindicatorview2.draw.DrawManager
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator

class IndicatorManager internal constructor(private val listener: Listener?) : ValueController.UpdateListener {

    private val drawManager: DrawManager = DrawManager()
    private val animationManager: AnimationManager

    internal interface Listener {
        fun onIndicatorUpdated()
    }

    init {
        this.animationManager = AnimationManager(drawManager.indicator(), this)
    }

    fun animate(): AnimationManager {
        return animationManager
    }

    fun indicator(): Indicator {
        return drawManager.indicator()
    }

    fun drawer(): DrawManager {
        return drawManager
    }

    override fun onValueUpdated(value: Value?) {
        drawManager.updateValue(value)
        listener?.onIndicatorUpdated()
    }
}
