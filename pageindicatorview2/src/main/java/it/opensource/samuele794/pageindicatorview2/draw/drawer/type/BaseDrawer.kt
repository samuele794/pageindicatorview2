package it.opensource.samuele794.pageindicatorview2.draw.drawer.type

import android.graphics.Paint
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator

open class BaseDrawer(var paint: Paint, var indicator: Indicator)
