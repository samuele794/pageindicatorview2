package it.opensource.samuele794.pageindicatorview2.animation.data.type

import it.opensource.samuele794.pageindicatorview2.animation.data.Value

class FillAnimationValue : ColorAnimationValue(), Value {

    var radius: Int = 0
    var radiusReverse: Int = 0

    var stroke: Int = 0
    var strokeReverse: Int = 0
}
