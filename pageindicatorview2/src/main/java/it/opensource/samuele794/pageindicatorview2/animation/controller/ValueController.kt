package it.opensource.samuele794.pageindicatorview2.animation.controller

import it.opensource.samuele794.pageindicatorview2.animation.data.Value
import it.opensource.samuele794.pageindicatorview2.animation.type.*

class ValueController(private val updateListener: UpdateListener?) {

    private lateinit var colorAnimation: ColorAnimation
    private lateinit var scaleAnimation: ScaleAnimation
    private lateinit var wormAnimation: WormAnimation
    private lateinit var slideAnimation: SlideAnimation
    private lateinit var fillAnimation: FillAnimation
    private lateinit var thinWormAnimation: ThinWormAnimation
    private lateinit var dropAnimation: DropAnimation
    private lateinit var swapAnimation: SwapAnimation
    private lateinit var scaleDownAnimation: ScaleDownAnimation

    interface UpdateListener {
        fun onValueUpdated(value: Value?)
    }

    fun color(): ColorAnimation {
        if (::colorAnimation.isInitialized.not()) {
            colorAnimation = ColorAnimation(updateListener)
        }

        return colorAnimation
    }

    fun scale(): ScaleAnimation {
        if (::scaleAnimation.isInitialized.not()) {
            scaleAnimation = ScaleAnimation(updateListener!!)
        }

        return scaleAnimation
    }

    fun worm(): WormAnimation {
        if (::wormAnimation.isInitialized.not()) {
            wormAnimation = WormAnimation(updateListener!!)
        }

        return wormAnimation
    }

    fun slide(): SlideAnimation {
        if (::slideAnimation.isInitialized.not()) {
            slideAnimation = SlideAnimation(updateListener!!)
        }

        return slideAnimation
    }

    fun fill(): FillAnimation {
        if (::fillAnimation.isInitialized.not()) {
            fillAnimation = FillAnimation(updateListener!!)
        }

        return fillAnimation
    }

    fun thinWorm(): ThinWormAnimation {
        if (::thinWormAnimation.isInitialized.not()) {
            thinWormAnimation = ThinWormAnimation(updateListener!!)
        }

        return thinWormAnimation
    }

    fun drop(): DropAnimation {
        if (::dropAnimation.isInitialized.not()) {
            dropAnimation = DropAnimation(updateListener!!)
        }

        return dropAnimation
    }

    fun swap(): SwapAnimation {
        if (::swapAnimation.isInitialized.not()) {
            swapAnimation = SwapAnimation(updateListener!!)
        }

        return swapAnimation
    }

    fun scaleDown(): ScaleDownAnimation {
        if (::scaleDownAnimation.isInitialized.not()) {
            scaleDownAnimation = ScaleDownAnimation(updateListener!!)
        }

        return scaleDownAnimation
    }
}
