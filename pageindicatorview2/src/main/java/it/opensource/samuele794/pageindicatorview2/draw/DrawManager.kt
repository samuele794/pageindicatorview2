package it.opensource.samuele794.pageindicatorview2.draw

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Pair
import android.view.MotionEvent
import it.opensource.samuele794.pageindicatorview2.animation.data.Value
import it.opensource.samuele794.pageindicatorview2.draw.controller.AttributeController
import it.opensource.samuele794.pageindicatorview2.draw.controller.DrawController
import it.opensource.samuele794.pageindicatorview2.draw.controller.MeasureController
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator

class DrawManager {

    private var indicator = Indicator()
    private val drawController = DrawController(indicator)
    private val measureController = MeasureController()
    private val attributeController = AttributeController(indicator)

    fun indicator(): Indicator {
        return indicator
    }

    fun setClickListener(listener: DrawController.ClickListener?) {
        drawController.setClickListener(listener)
    }

    fun touch(event: MotionEvent?) {
        drawController.touch(event)
    }

    fun updateValue(value: Value?) {
        drawController.updateValue(value)
    }

    fun draw(canvas: Canvas) {
        drawController.draw(canvas)
    }

    fun measureViewSize(widthMeasureSpec: Int, heightMeasureSpec: Int): Pair<Int, Int> {
        return measureController.measureViewSize(indicator, widthMeasureSpec, heightMeasureSpec)
    }

    fun initAttributes(context: Context, attrs: AttributeSet?) {
        attributeController.init(context, attrs)
    }
}
