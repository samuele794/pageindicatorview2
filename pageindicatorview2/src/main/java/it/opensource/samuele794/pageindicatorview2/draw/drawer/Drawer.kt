package it.opensource.samuele794.pageindicatorview2.draw.drawer

import android.graphics.Canvas
import android.graphics.Paint
import it.opensource.samuele794.pageindicatorview2.animation.data.Value
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator
import it.opensource.samuele794.pageindicatorview2.draw.drawer.type.*

class Drawer(private val indicator: Indicator) {

    val paint = Paint().apply {
        style = Paint.Style.FILL
        isAntiAlias = true
    }

    private lateinit var basicDrawer: BasicDrawer
    private lateinit var colorDrawer: ColorDrawer
    private lateinit var scaleDrawer: ScaleDrawer
    private lateinit var wormDrawer: WormDrawer
    private lateinit var slideDrawer: SlideDrawer
    private lateinit var fillDrawer: FillDrawer
    private lateinit var thinWormDrawer: ThinWormDrawer
    private lateinit var dropDrawer: DropDrawer
    private lateinit var swapDrawer: SwapDrawer
    private lateinit var scaleDownDrawer: ScaleDownDrawer

    private var position: Int = 0
    private var coordinateX: Int = 0
    private var coordinateY: Int = 0

    fun setup(position: Int, coordinateX: Int, coordinateY: Int) {
        this.position = position
        this.coordinateX = coordinateX
        this.coordinateY = coordinateY
    }

    fun drawBasic(canvas: Canvas, isSelectedItem: Boolean) {
        if (::colorDrawer.isInitialized.not() and ::basicDrawer.isInitialized.not()) {
            basicDrawer = BasicDrawer(paint, indicator)
            colorDrawer = ColorDrawer(paint, indicator)
        }
        basicDrawer.draw(canvas, position, isSelectedItem, coordinateX, coordinateY)
    }

    fun drawColor(canvas: Canvas, value: Value) {
        if (::colorDrawer.isInitialized.not())
            colorDrawer = ColorDrawer(paint, indicator)

        colorDrawer.draw(canvas, value, position, coordinateX, coordinateY)
    }

    fun drawScale(canvas: Canvas, value: Value) {
        if (::scaleDrawer.isInitialized.not())
            scaleDrawer = ScaleDrawer(paint, indicator)
        scaleDrawer.draw(canvas, value, position, coordinateX, coordinateY)
    }

    fun drawWorm(canvas: Canvas, value: Value) {
        if (::wormDrawer.isInitialized.not())
            wormDrawer = WormDrawer(paint, indicator)
        wormDrawer.draw(canvas, value, coordinateX, coordinateY)
    }

    fun drawSlide(canvas: Canvas, value: Value) {
        if (::slideDrawer.isInitialized.not())
            slideDrawer = SlideDrawer(paint, indicator)
        slideDrawer.draw(canvas, value, coordinateX, coordinateY)
    }

    fun drawFill(canvas: Canvas, value: Value) {
        if (::fillDrawer.isInitialized.not())
            fillDrawer = FillDrawer(paint, indicator)
        fillDrawer.draw(canvas, value, position, coordinateX, coordinateY)
    }

    fun drawThinWorm(canvas: Canvas, value: Value) {
        if (::thinWormDrawer.isInitialized.not())
            thinWormDrawer = ThinWormDrawer(paint, indicator)
        thinWormDrawer.draw(canvas, value, coordinateX, coordinateY)
    }

    fun drawDrop(canvas: Canvas, value: Value) {
        if (::dropDrawer.isInitialized.not())
            dropDrawer = DropDrawer(paint, indicator)
        dropDrawer.draw(canvas, value, coordinateX, coordinateY)
    }

    fun drawSwap(canvas: Canvas, value: Value) {
        if (::scaleDrawer.isInitialized.not())
            swapDrawer = SwapDrawer(paint, indicator)
        swapDrawer.draw(canvas, value, position, coordinateX, coordinateY)
    }

    fun drawScaleDown(canvas: Canvas, value: Value) {
        if (::scaleDownDrawer.isInitialized.not())
            scaleDownDrawer = ScaleDownDrawer(paint, indicator)
        scaleDownDrawer.draw(canvas, value, position, coordinateX, coordinateY)
    }
}
