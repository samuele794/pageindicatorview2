package it.opensource.samuele794.pageindicatorview2.animation.data.type

import it.opensource.samuele794.pageindicatorview2.animation.data.Value

open class ColorAnimationValue : Value {

    var color: Int = 0
    var colorReverse: Int = 0
}
