package it.opensource.samuele794.pageindicatorview2.animation.controller

import it.opensource.samuele794.pageindicatorview2.animation.type.AnimationType
import it.opensource.samuele794.pageindicatorview2.animation.type.BaseAnimation
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator
import it.opensource.samuele794.pageindicatorview2.draw.data.Orientation
import it.opensource.samuele794.pageindicatorview2.utils.CoordinatesUtils

class AnimationController(private val indicator: Indicator, private val listener: ValueController.UpdateListener) {

    private val valueController: ValueController = ValueController(listener)

    private var runningAnimation: BaseAnimation<*>? = null

    private var progress: Float = 0.toFloat()
    private var isInteractive: Boolean = false

    fun interactive(progress: Float) {
        this.isInteractive = true
        this.progress = progress
        animate()
    }

    fun basic() {
        this.isInteractive = false
        this.progress = 0f
        animate()
    }

    fun end() {
        if (runningAnimation != null) {
            runningAnimation!!.end()
        }
    }

    private fun animate() {
        when (indicator.animationType) {
            AnimationType.NONE -> listener.onValueUpdated(null)

            AnimationType.COLOR -> colorAnimation()

            AnimationType.SCALE -> scaleAnimation()

            AnimationType.WORM -> wormAnimation()

            AnimationType.FILL -> fillAnimation()

            AnimationType.SLIDE -> slideAnimation()

            AnimationType.THIN_WORM -> thinWormAnimation()

            AnimationType.DROP -> dropAnimation()

            AnimationType.SWAP -> swapAnimation()

            AnimationType.SCALE_DOWN -> scaleDownAnimation()
        }
    }

    private fun colorAnimation() {
        val selectedColor = indicator.selectedColor
        val unselectedColor = indicator.unselectedColor
        val animationDuration = indicator.animationDuration

        val animation = valueController
                .color()
                .with(unselectedColor, selectedColor)
                .duration(animationDuration)

        if (isInteractive) {
            animation.progress(progress)
        } else {
            animation.start()
        }

        runningAnimation = animation
    }

    private fun scaleAnimation() {
        val selectedColor = indicator.selectedColor
        val unselectedColor = indicator.unselectedColor
        val radiusPx = indicator.radius
        val scaleFactor = indicator.scaleFactor
        val animationDuration = indicator.animationDuration

        val animation = valueController
                .scale()
                .with(unselectedColor, selectedColor, radiusPx, scaleFactor)
                .duration(animationDuration)

        if (isInteractive) {
            animation.progress(progress)
        } else {
            animation.start()
        }

        runningAnimation = animation
    }

    private fun wormAnimation() {
        val fromPosition = if (indicator.isInteractiveAnimation) indicator.selectedPosition else indicator.lastSelectedPosition
        val toPosition = if (indicator.isInteractiveAnimation) indicator.selectingPosition else indicator.selectedPosition

        val from = CoordinatesUtils.getCoordinate(indicator, fromPosition)
        val to = CoordinatesUtils.getCoordinate(indicator, toPosition)
        val isRightSide = toPosition > fromPosition

        val radiusPx = indicator.radius
        val animationDuration = indicator.animationDuration

        val animation = valueController
                .worm()
                .with(from, to, radiusPx, isRightSide)
                .duration(animationDuration)

        if (isInteractive) {
            animation.progress(progress)
        } else {
            animation.start()
        }

        runningAnimation = animation
    }

    private fun slideAnimation() {
        val fromPosition = if (indicator.isInteractiveAnimation) indicator.selectedPosition else indicator.lastSelectedPosition
        val toPosition = if (indicator.isInteractiveAnimation) indicator.selectingPosition else indicator.selectedPosition

        val from = CoordinatesUtils.getCoordinate(indicator, fromPosition)
        val to = CoordinatesUtils.getCoordinate(indicator, toPosition)
        val animationDuration = indicator.animationDuration

        val animation = valueController
                .slide()
                .with(from, to)
                .duration(animationDuration)

        if (isInteractive) {
            animation.progress(progress)
        } else {
            animation.start()
        }

        runningAnimation = animation
    }

    private fun fillAnimation() {
        val selectedColor = indicator.selectedColor
        val unselectedColor = indicator.unselectedColor
        val radiusPx = indicator.radius
        val strokePx = indicator.stroke
        val animationDuration = indicator.animationDuration

        val animation = valueController
                .fill()
                .with(unselectedColor, selectedColor, radiusPx, strokePx)
                .duration(animationDuration)

        if (isInteractive) {
            animation.progress(progress)
        } else {
            animation.start()
        }

        runningAnimation = animation
    }

    private fun thinWormAnimation() {
        val fromPosition = if (indicator.isInteractiveAnimation) indicator.selectedPosition else indicator.lastSelectedPosition
        val toPosition = if (indicator.isInteractiveAnimation) indicator.selectingPosition else indicator.selectedPosition

        val from = CoordinatesUtils.getCoordinate(indicator, fromPosition)
        val to = CoordinatesUtils.getCoordinate(indicator, toPosition)
        val isRightSide = toPosition > fromPosition

        val radiusPx = indicator.radius
        val animationDuration = indicator.animationDuration

        val animation = valueController
                .thinWorm()
                .with(from, to, radiusPx, isRightSide)
                .duration(animationDuration)

        if (isInteractive) {
            animation.progress(progress)
        } else {
            animation.start()
        }

        runningAnimation = animation
    }

    private fun dropAnimation() {
        val fromPosition = if (indicator.isInteractiveAnimation) indicator.selectedPosition else indicator.lastSelectedPosition
        val toPosition = if (indicator.isInteractiveAnimation) indicator.selectingPosition else indicator.selectedPosition

        val widthFrom = CoordinatesUtils.getCoordinate(indicator, fromPosition)
        val widthTo = CoordinatesUtils.getCoordinate(indicator, toPosition)

        val paddingTop = indicator.paddingTop
        val paddingLeft = indicator.paddingLeft
        val padding = if (indicator.orientation == Orientation.HORIZONTAL) paddingTop else paddingLeft

        val radius = indicator.radius
        val heightFrom = radius * 3 + padding
        val heightTo = radius + padding

        val animationDuration = indicator.animationDuration

        val animation = valueController
                .drop()
                .duration(animationDuration)
                .with(widthFrom, widthTo, heightFrom, heightTo, radius)

        if (isInteractive) {
            animation.progress(progress)
        } else {
            animation.start()
        }

        runningAnimation = animation
    }

    private fun swapAnimation() {
        val fromPosition = if (indicator.isInteractiveAnimation) indicator.selectedPosition else indicator.lastSelectedPosition
        val toPosition = if (indicator.isInteractiveAnimation) indicator.selectingPosition else indicator.selectedPosition

        val from = CoordinatesUtils.getCoordinate(indicator, fromPosition)
        val to = CoordinatesUtils.getCoordinate(indicator, toPosition)
        val animationDuration = indicator.animationDuration

        val animation = valueController
                .swap()
                .with(from, to)
                .duration(animationDuration)

        if (isInteractive) {
            animation.progress(progress)
        } else {
            animation.start()
        }

        runningAnimation = animation
    }

    private fun scaleDownAnimation() {
        val selectedColor = indicator.selectedColor
        val unselectedColor = indicator.unselectedColor
        val radiusPx = indicator.radius
        val scaleFactor = indicator.scaleFactor
        val animationDuration = indicator.animationDuration

        val animation = valueController
                .scaleDown()
                .with(unselectedColor, selectedColor, radiusPx, scaleFactor)
                .duration(animationDuration)

        if (isInteractive) {
            animation.progress(progress)
        } else {
            animation.start()
        }

        runningAnimation = animation
    }
}

