package it.opensource.samuele794.pageindicatorview2.animation.type

import android.animation.Animator
import android.animation.ValueAnimator
import it.opensource.samuele794.pageindicatorview2.animation.controller.ValueController

abstract class BaseAnimation<T : Animator>(protected var listener: ValueController.UpdateListener?) {
    protected var animationDuration = DEFAULT_ANIMATION_TIME.toLong()
    protected var animator: T? = null

    init {
        animator = createAnimator()
    }

    abstract fun createAnimator(): T

    abstract fun progress(progress: Float): BaseAnimation<*>

    open fun duration(duration: Long): BaseAnimation<*> {
        animationDuration = duration

        if (animator is ValueAnimator) {
            animator!!.duration = animationDuration
        }

        return this
    }

    fun start() {
        if (animator != null && !animator!!.isRunning) {
            animator!!.start()
        }
    }

    fun end() {
        if (animator != null && animator!!.isStarted) {
            animator!!.end()
        }
    }

    companion object {

        val DEFAULT_ANIMATION_TIME = 350
    }
}
