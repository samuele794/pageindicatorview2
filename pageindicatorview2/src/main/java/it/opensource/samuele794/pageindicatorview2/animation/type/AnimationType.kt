package it.opensource.samuele794.pageindicatorview2.animation.type

enum class AnimationType {
    NONE, COLOR, SCALE, WORM, SLIDE, FILL, THIN_WORM, DROP, SWAP, SCALE_DOWN
}
