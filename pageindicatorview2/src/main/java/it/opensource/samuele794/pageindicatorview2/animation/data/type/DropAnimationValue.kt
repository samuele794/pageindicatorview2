package it.opensource.samuele794.pageindicatorview2.animation.data.type

import it.opensource.samuele794.pageindicatorview2.animation.data.Value

class DropAnimationValue : Value {

    var width: Int = 0
    var height: Int = 0
    var radius: Int = 0
}
