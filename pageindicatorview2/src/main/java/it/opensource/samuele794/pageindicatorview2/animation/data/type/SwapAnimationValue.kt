package it.opensource.samuele794.pageindicatorview2.animation.data.type

import it.opensource.samuele794.pageindicatorview2.animation.data.Value

class SwapAnimationValue : Value {

    var coordinate: Int = 0
    var coordinateReverse: Int = 0
}
