package it.opensource.samuele794.pageindicatorview2.animation.data.type

import it.opensource.samuele794.pageindicatorview2.animation.data.Value

class SlideAnimationValue : Value {

    var coordinate: Int = 0
}
