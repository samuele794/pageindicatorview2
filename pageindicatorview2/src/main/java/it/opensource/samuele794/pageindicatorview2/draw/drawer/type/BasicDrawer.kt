package it.opensource.samuele794.pageindicatorview2.draw.drawer.type

import android.graphics.Canvas
import android.graphics.Paint
import it.opensource.samuele794.pageindicatorview2.animation.type.AnimationType
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator

class BasicDrawer(paint: Paint, indicator: Indicator) : BaseDrawer(paint, indicator) {

    private val strokePaint: Paint = Paint().apply {
        style = Paint.Style.STROKE
        isAntiAlias = true
        strokeWidth = indicator.stroke.toFloat()
    }

    fun draw(
            canvas: Canvas,
            position: Int,
            isSelectedItem: Boolean,
            coordinateX: Int,
            coordinateY: Int) {

        var radius = indicator.radius.toFloat()
        val strokePx = indicator.stroke
        val scaleFactor = indicator.scaleFactor

        val selectedColor = indicator.selectedColor
        val unselectedColor = indicator.unselectedColor
        val selectedPosition = indicator.selectedPosition
        val animationType = indicator.animationType

        when (animationType) {
            AnimationType.SCALE -> {
                if (isSelectedItem.not())
                    radius *= scaleFactor
            }

            AnimationType.SCALE_DOWN -> {
                if (isSelectedItem) {
                    radius *= scaleFactor
                }
            }
        }

        var color = unselectedColor
        if (position == selectedPosition) {
            color = selectedColor
        }

        val paint: Paint
        if (animationType == AnimationType.FILL && position != selectedPosition) {
            paint = strokePaint
            paint.strokeWidth = strokePx.toFloat()
        } else {
            paint = this.paint
        }

        paint.color = color
        canvas.drawCircle(coordinateX.toFloat(), coordinateY.toFloat(), radius, paint)
    }
}
