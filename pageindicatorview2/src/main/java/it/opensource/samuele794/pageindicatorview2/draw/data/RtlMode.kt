package it.opensource.samuele794.pageindicatorview2.draw.data

enum class RtlMode {
    ON, OFF, AUTO
}