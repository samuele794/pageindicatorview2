package it.opensource.samuele794.pageindicatorview2.animation.type

import android.animation.IntEvaluator
import android.animation.PropertyValuesHolder
import it.opensource.samuele794.pageindicatorview2.animation.controller.ValueController

class ScaleDownAnimation(listener: ValueController.UpdateListener) : ScaleAnimation(listener) {

    override fun createScalePropertyHolder(isReverse: Boolean): PropertyValuesHolder {
        val propertyName: String
        val startRadiusValue: Int
        val endRadiusValue: Int

        if (isReverse) {
            propertyName = ANIMATION_SCALE_REVERSE
            startRadiusValue = (radius * scaleFactor).toInt()
            endRadiusValue = radius
        } else {
            propertyName = ANIMATION_SCALE
            startRadiusValue = radius
            endRadiusValue = (radius * scaleFactor).toInt()
        }

        val holder = PropertyValuesHolder.ofInt(propertyName, startRadiusValue, endRadiusValue)
        holder.setEvaluator(IntEvaluator())

        return holder
    }
}

