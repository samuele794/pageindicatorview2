package it.opensource.samuele794.pageindicatorview2.animation

import it.opensource.samuele794.pageindicatorview2.animation.controller.AnimationController
import it.opensource.samuele794.pageindicatorview2.animation.controller.ValueController
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator

class AnimationManager(indicator: Indicator, listener: ValueController.UpdateListener) {

    private val animationController = AnimationController(indicator, listener)


    fun basic() {
        animationController.end()
        animationController.basic()
    }

    fun interactive(progress: Float) {
        animationController.interactive(progress)
    }

    fun end() {
        animationController.end()
    }
}
