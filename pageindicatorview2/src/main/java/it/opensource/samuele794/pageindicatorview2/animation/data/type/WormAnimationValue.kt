package it.opensource.samuele794.pageindicatorview2.animation.data.type

import it.opensource.samuele794.pageindicatorview2.animation.data.Value

open class WormAnimationValue : Value {

    var rectStart: Int = 0
    var rectEnd: Int = 0
}
