package it.opensource.samuele794.pageindicatorview2.draw.drawer.type

import android.graphics.Canvas
import android.graphics.Paint
import it.opensource.samuele794.pageindicatorview2.animation.data.Value
import it.opensource.samuele794.pageindicatorview2.animation.data.type.ThinWormAnimationValue
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator
import it.opensource.samuele794.pageindicatorview2.draw.data.Orientation

class ThinWormDrawer(paint: Paint, indicator: Indicator) : WormDrawer(paint, indicator) {

    override fun draw(
            canvas: Canvas,
            value: Value,
            coordinateX: Int,
            coordinateY: Int) {

        if (value !is ThinWormAnimationValue) {
            return
        }

        val rectStart = value.rectStart
        val rectEnd = value.rectEnd
        val height = value.height / 2

        val radius = indicator.radius
        val unselectedColor = indicator.unselectedColor
        val selectedColor = indicator.selectedColor

        if (indicator.orientation == Orientation.HORIZONTAL) {
            rect.apply {
                left = rectStart.toFloat()
                right = rectEnd.toFloat()
                top = (coordinateY - height).toFloat()
                bottom = (coordinateY + height).toFloat()
            }

        } else {
            rect.apply {
                left = (coordinateX - height).toFloat()
                right = (coordinateX + height).toFloat()
                top = rectStart.toFloat()
                bottom = rectEnd.toFloat()
            }
        }

        paint.color = unselectedColor
        canvas.drawCircle(coordinateX.toFloat(), coordinateY.toFloat(), radius.toFloat(), paint)

        paint.color = selectedColor
        canvas.drawRoundRect(rect, radius.toFloat(), radius.toFloat(), paint)
    }
}
