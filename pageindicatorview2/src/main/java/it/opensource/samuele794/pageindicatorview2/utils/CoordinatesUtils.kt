package it.opensource.samuele794.pageindicatorview2.utils

import android.util.Pair
import it.opensource.samuele794.pageindicatorview2.animation.type.AnimationType
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator
import it.opensource.samuele794.pageindicatorview2.draw.data.Orientation

object CoordinatesUtils {

    fun getCoordinate(indicator: Indicator?, position: Int): Int {
        indicator?.let {
            return if (it.orientation == Orientation.HORIZONTAL)
                getXCoordinate(indicator, position)
            else
                getYCoordinate(indicator, position)
        }
        return 0
    }

    fun getXCoordinate(indicator: Indicator?, position: Int): Int {
        indicator?.let {
            var coordinate: Int = if (it.orientation == Orientation.HORIZONTAL)
                getHorizontalCoordinate(indicator, position)
            else
                getVerticalCoordinate(indicator)

            coordinate += indicator.paddingLeft

            return coordinate
        }

        return 0
    }

    fun getYCoordinate(indicator: Indicator?, position: Int): Int {
        if (indicator == null) {
            return 0
        }

        var coordinate: Int
        if (indicator.orientation == Orientation.HORIZONTAL) {
            coordinate = getVerticalCoordinate(indicator)
        } else {
            coordinate = getHorizontalCoordinate(indicator, position)
        }

        coordinate += indicator.paddingTop
        return coordinate
    }

    fun getPosition(indicator: Indicator?, x: Float, y: Float): Int {

        indicator?.let {
            val lengthCoordinate: Float
            val heightCoordinate: Float

            if (indicator.orientation == Orientation.HORIZONTAL) {
                lengthCoordinate = x
                heightCoordinate = y
            } else {
                lengthCoordinate = y
                heightCoordinate = x
            }

            return getFitPosition(indicator, lengthCoordinate, heightCoordinate)
        }

        return -1
    }

    private fun getFitPosition(indicator: Indicator, lengthCoordinate: Float, heightCoordinate: Float): Int {
        val count = indicator.count
        val radius = indicator.radius
        val stroke = indicator.stroke
        val padding = indicator.padding

        val height = if (indicator.orientation == Orientation.HORIZONTAL) indicator.height else indicator.width
        var length = 0

        for (i in 0 until count) {
            val indicatorPadding = if (i > 0) padding else padding / 2
            val startValue = length

            length += radius * 2 + stroke / 2 + indicatorPadding
            val endValue = length

            val fitLength = lengthCoordinate >= startValue && lengthCoordinate <= endValue
            val fitHeight = heightCoordinate >= 0 && heightCoordinate <= height

            if (fitLength && fitHeight) {
                return i
            }
        }

        return -1
    }

    private fun getHorizontalCoordinate(indicator: Indicator, position: Int): Int {
        val count = indicator.count
        val radius = indicator.radius
        val stroke = indicator.stroke
        val padding = indicator.padding

        var coordinate = 0
        for (i in 0 until count) {
            coordinate += radius + stroke / 2

            if (position == i) {
                return coordinate
            }

            coordinate += radius + padding + stroke / 2
        }

        if (indicator.animationType == AnimationType.DROP) {
            coordinate += radius * 2
        }

        return coordinate
    }

    private fun getVerticalCoordinate(indicator: Indicator): Int {
        return if (indicator.animationType == AnimationType.DROP) {
            indicator.radius * 3
        } else {
            indicator.radius
        }

    }

    fun getProgress(indicator: Indicator, position: Int, positionOffset: Float, isRtl: Boolean): Pair<Int, Float> {
        var position = position
        val count = indicator.count
        var selectedPosition = indicator.selectedPosition

        if (isRtl) {
            position = count - 1 - position
        }

        if (position < 0) {
            position = 0

        } else if (position > count - 1) {
            position = count - 1
        }

        val isRightOverScrolled = position > selectedPosition
        val isLeftOverScrolled: Boolean

        if (isRtl) {
            isLeftOverScrolled = position - 1 < selectedPosition
        } else {
            isLeftOverScrolled = position + 1 < selectedPosition
        }

        if (isRightOverScrolled || isLeftOverScrolled) {
            selectedPosition = position
            indicator.selectedPosition = selectedPosition
        }

        val slideToRightSide = selectedPosition == position && positionOffset != 0f
        val selectingPosition: Int
        var selectingProgress: Float

        if (slideToRightSide) {
            selectingPosition = if (isRtl) position - 1 else position + 1
            selectingProgress = positionOffset

        } else {
            selectingPosition = position
            selectingProgress = 1 - positionOffset
        }

        if (selectingProgress > 1) {
            selectingProgress = 1f

        } else if (selectingProgress < 0) {
            selectingProgress = 0f
        }

        return Pair(selectingPosition, selectingProgress)
    }
}
