package it.opensource.samuele794.pageindicatorview2.draw.drawer.type

import android.graphics.Canvas
import android.graphics.Paint
import it.opensource.samuele794.pageindicatorview2.animation.data.Value
import it.opensource.samuele794.pageindicatorview2.animation.data.type.ColorAnimationValue
import it.opensource.samuele794.pageindicatorview2.draw.data.Indicator

class ColorDrawer(paint: Paint, indicator: Indicator) : BaseDrawer(paint, indicator) {

    fun draw(canvas: Canvas,
             value: Value,
             position: Int,
             coordinateX: Int,
             coordinateY: Int) {

        if (value !is ColorAnimationValue) {
            return
        }

        val radius = indicator.radius.toFloat()
        var color = indicator.selectedColor

        val selectedPosition = indicator.selectedPosition
        val selectingPosition = indicator.selectingPosition
        val lastSelectedPosition = indicator.lastSelectedPosition

        if (indicator.isInteractiveAnimation) {
            if (position == selectingPosition) {
                color = value.color

            } else if (position == selectedPosition) {
                color = value.colorReverse
            }

        } else {
            if (position == selectedPosition) {
                color = value.color

            } else if (position == lastSelectedPosition) {
                color = value.colorReverse
            }
        }

        paint.color = color
        canvas.drawCircle(coordinateX.toFloat(), coordinateY.toFloat(), radius, paint)
    }
}
