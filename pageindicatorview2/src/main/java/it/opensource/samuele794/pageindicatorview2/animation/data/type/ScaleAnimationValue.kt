package it.opensource.samuele794.pageindicatorview2.animation.data.type

import it.opensource.samuele794.pageindicatorview2.animation.data.Value

class ScaleAnimationValue : ColorAnimationValue(), Value {

    var radius: Int = 0
    var radiusReverse: Int = 0
}
