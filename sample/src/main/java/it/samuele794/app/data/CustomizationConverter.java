package it.samuele794.app.data;

import it.opensource.samuele794.pageindicatorview2.animation.type.AnimationType;
import it.opensource.samuele794.pageindicatorview2.draw.data.Orientation;
import it.opensource.samuele794.pageindicatorview2.draw.data.RtlMode;

public class CustomizationConverter {

    public static AnimationType getAnimationType(int position) {
        switch (position) {
            case 0:
                return AnimationType.NONE;

            case 1:
                return AnimationType.COLOR;

            case 2:
                return AnimationType.SCALE;

            case 3:
                return AnimationType.WORM;

            case 4:
                return AnimationType.SLIDE;

            case 5:
                return AnimationType.FILL;

            case 6:
                return AnimationType.THIN_WORM;

            case 7:
                return AnimationType.DROP;

            case 8:
                return AnimationType.SWAP;

            case 9:
                return AnimationType.SCALE_DOWN;

            default:
                return AnimationType.NONE;
        }
    }

    public static Orientation getOrientation(int position) {
        switch (position) {
            case 0:
                return Orientation.HORIZONTAL;

            case 1:
                return Orientation.VERTICAL;

            default:
                return Orientation.HORIZONTAL;
        }
    }

    public static RtlMode getRtlMode(int position) {
        switch (position) {
            case 0:
                return RtlMode.ON;

            case 1:
                return RtlMode.OFF;

            case 2:
                return RtlMode.AUTO;

            default:
                return RtlMode.OFF;
        }
    }

}
