package it.samuele794.app.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import it.samuele794.app.R

class HomeAdapter2(private val context: Context) : RecyclerView.Adapter<HomeAdapter2.ViewHolder>() {


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.page2, parent, false))
    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (position) {
            0 -> holder.itemView.setBackgroundColor(context.resources.getColor(R.color.blue_400, context.theme))
            1 -> holder.itemView.setBackgroundColor(context.resources.getColor(R.color.blue_200, context.theme))
        }
    }

    /* private var viewList: ArrayList<View> = arrayListOf()

     override fun instantiateItem(collection: ViewGroup, position: Int): Any {
         val view = viewList[position]
         collection.addView(view)
         return view
     }

     override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
         collection.removeView(view as View)
     }

     override fun getCount(): Int {
         return viewList.size
     }

     override fun isViewFromObject(view: View, obj: Any): Boolean {
         return view == obj
     }

     override fun getItemPosition(obj: Any): Int {
         return POSITION_NONE
     }

     fun setData(list: List<View>?) {
         this.viewList.clear()
         if (list != null && list.isNotEmpty()) {
             this.viewList.addAll(list)
         }

         notifyDataSetChanged()
     }

     fun getData(): List<View> {
         return viewList
     }*/
}